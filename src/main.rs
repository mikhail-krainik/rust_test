extern crate ofertus_server_lib as ofertus_server;
pub mod cli;

use ofertus_server::cli::ArgumentParser;
use std::process;

fn main() {
    let argument_parser = ArgumentParser::new(
            "Ofertus Server",
            env!("CARGO_PKG_VERSION"),
            env!("CARGO_PKG_AUTHORS"),
            "Ofertus Server CLI Tool",
    );

    let config = argument_parser.get_arguments_config().unwrap_or_else(|error|{
        eprintln!("Problem parsing arguments: {}", error);
        process::exit(1);
    });

    ofertus_server::run(config);
}
