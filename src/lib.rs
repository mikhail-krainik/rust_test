mod server;
pub mod cli;

use cli::config::Config;

pub fn run(config: Config) -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let socket_address = format!("{}:{}", "127.0.0.1", config.port);
    log::info!(
        "Running the server process on address: http://{}",
        socket_address
    );

    actix_web::server::new(|| {
        actix_web::App::new()
            .resource("/", |r| r.f(server::index))
    })
    .workers(config.thread_number)
    .bind(socket_address)
    .unwrap()
    .run();

    Ok(())
}
