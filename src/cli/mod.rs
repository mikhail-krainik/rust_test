#![allow(dead_code)]
pub mod config;

use clap::Arg;
use config::Config;

pub struct ArgumentParser {
    clap_app: clap::App<'static, 'static>,
}

impl ArgumentParser {
    pub fn new(
        app_name: &'static str,
        version: &'static str,
        author: &'static str,
        about: &'static str,
    ) -> ArgumentParser {
        let clap_app = clap::App::new(app_name)
            .version(version)
            .author(author)
            .about(about)
            .arg(
                Arg::with_name("port")
                    .short("p")
                    .long("port")
                    .value_name("PORT")
                    .required(false)
                    .takes_value(true)
                    .help("Port to listen. Defaults to 8080"),
            )
            .arg(
                Arg::with_name("thread_number")
                    .short("t")
                    .long("threads")
                    .value_name("THREAD_NUMBER")
                    .required(false)
                    .takes_value(true)
                    .help("Number of threads to use. Defautls to system available"),
            );
        ArgumentParser { clap_app }
    }

    pub fn get_arguments_config(&self) -> Result<Config, &'static str> {
        let arguments = self.clap_app.clone().get_matches();
        Config::new(
            arguments.value_of("port"),
            arguments.value_of("thread_number"),
        )
    }
}
