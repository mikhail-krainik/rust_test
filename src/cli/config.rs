pub struct Config {
    pub port: usize,
    pub thread_number: usize,
}

impl Config {
    pub fn new(port: Option<&str>, thread_number: Option<&str>) -> Result<Config, &'static str> {
        let port = match port {
            Some(port) => match port.parse() {
                Ok(port) => port,
                Err(_) => return Err("Could not parse the port"),
            },
            None => 8080,
        };
        let thread_number = match thread_number {
            Some(thread_number) => match thread_number.parse() {
                Ok(thread_number) => thread_number,
                Err(_) => return Err("Could not parse the Thread number"),
            },
            None => num_cpus::get(),
        };
        Ok(Config {
            port,
            thread_number,
        })
    }
}